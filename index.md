---
layout: home
---

Holaaaa! You can edit or remove this text in ``index.md``.

To change the welcome title edit in ``_layouts/home.html``.

Here is also an example of a figure with a caption. If you don't need the caption you can simply use the image syntax:

``![](path/to/image.jpg)``

{% figure caption: "Figure with caption [Source](https://unsplash.com/photos/rhPdU1XuS1k)" %}
![](images/hero.jpg)
{% endfigure %}


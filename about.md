---
layout: page
title: About
permalink: /about/
---

Change jekyll version to `3.8.4` in ``Gemfile`` (not ``Gemfile.lock``!!!). It should look like this:

```
gem "jekyll", "~> 3.8.4"
```

then run ``bundle install``, it's going to shown an error about a wrong version of jekyll, then run ``bundle update jekyll``

once that is finished ``bundle exec jekyll serve``




This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

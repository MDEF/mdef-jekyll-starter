
## Jekyll

This site is using following Jekyll plugins:

- [jekyll_figure](https://github.com/lmullen/jekyll_figure)
- [jekyll-postfiles](https://github.com/nhoizey/jekyll-postfiles)
- [jekyll-feed](https://github.com/jekyll/jekyll-feed)

### Install Jekyll

[Follow instruction on Jekyll's website.](https://jekyllrb.com/docs/installation/)


### Running Jekyll

To run Jekyll locally run a following command:

```
bundle exec jekyll serve
```

### Fixing Jekyll issues on Mac

Sometimes there might be issues with installing Jekyll on Mac which are related to the system-wide Ruby that comes preinstalled with Macs and also different version of Jekyll.

Installing a custom version of Ruby should fix the issue:

#### 1. Install Homebrew
[**Homebrew** is a package manager for Mac](https://brew.sh/)

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

```

#### 2. Install rbenv

First you need a Ruby version manager. Two popular ones are [**rvm**](https://rvm.io/) and [**rbenv**](https://github.com/rbenv/rbenv). We will use **rbenv** in our example.

First install and setup rbenv:

```
brew install rbenv
```

```
 rbenv init
```

Then close your Terminal window and open a new one so your changes take effect.

Verify that rbenv is properly set up using this rbenv-doctor script:

```
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
```

You might need to edit your `` ~/.bash_profile``, make sure it contains the following lines:

```
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
```

(you will need to restart your terminal for changes to take effect)


#### 3. Install custom version of Ruby

Now it's time to install custom version of Ruby. To run Jekyll you need a Ruby version 2.2.5 or above. Here I choose 2.4.1 just because that is what I got running at the moment. Feel free to install newer versions:

```
rbenv install 2.4.1
```

And finally you need to instruct your system to start using your newly isntalled Ruby version:

```
rbenv global 2.4.1
```

#### 4. Check your Ruby version [! IMPORTANT]

You can check which Ruby version you are using by running ``which ruby``

It should return something along the lines of  ``/Users/[your-user]/.rbenv/versions/ruby-2.4.1/bin/ruby``

If it returns ``/usr/local/bin/ruby`` than your system is still using a pre-installed version of Ruby. In that case you need to revisit some of the previous steps and make sure that rbenv is functioning properly.

#### 5. Reinstall Jekyll

Now follow the original [instruction for installing Jekyll](https://jekyllrb.com/docs/installation/). In short you need to:

- navigate to the directory with your Jekyll site and run ``bundle install``
- you might get a warning about a wrong version of Jekyll, in that case just run: ``bundle update jekyll``
- now you should be able to run Jekyll by running: ``bundle exec jekyll serve``

## Project structure

The starter is based on the following:

| Language                    | Purpose                       | File extension(s)                   |
|-----------------------------|-------------------------------|-------------------------------------|
|  [HTML](https://en.wikipedia.org/wiki/HTML) | defines the semantic/content structure, *not for styling!* | `.html`                       |
| [Liquid](https://shopify.github.io/liquid/)         | feed data and enable logical operators in html , use curly brackets `{{ }}` and `{% %}` inside HTML            | contained in `.html`                   |
| [SASS](https://sass-lang.com/)                     | CSS extension that lets you split your styles into smaller modules for easier organisation, also lets you use custom variables and functions  | `.scss` |
| [Markdown](https://en.wikipedia.org/wiki/Markdown) | write HTML using plain text | `.md`|
| [YAML](https://en.wikipedia.org/wiki/YAML) | used for configuration files | `.yml` |


### Files and directories

- **`_includes/`** - reusable chunks of HTML that can be included in templates
 - `footer.html` - footer area of every page
 - `head.html` - contents of `<head>` HTML tag, not rendered on the page, used to reference stylesheets, fonts and for meta information
 - `header.html`- header of every page, containts logo and navigation
- **`_layouts/`** - content templates
 - `default.html`- default template referencing head, header, footer includes and defining a `{{content}}` area for putting in contents of other template files 
 - `home.html`- homepage template 
 - `page.html` - default template for pages (if no other template is given) 
 - `post.html`- template for posts (i.e. reflections)
 - `reflections.html`- template for reflections page, contains Liquid loop for getting all the item from *reflections* collection
- **`_reflections/`** - contains your markdown files for  *reflections* collection defined in Jekyll configuration file (bellow)
- **`_sass/`** - stylesheets for styling your site
    - `components/` 
        - `_footer.scss` - styles for footer area
        - `_header.scss`- styles for header area
        - `_post.scss` - styles for posts
    - `utilities/`
        - `_base.scss` - default styling for base HTML elements
        - `_helpers.scss`- helper classes for quick styling of text, spacing etc.
        - `_layout.scss` - classes related to layout
        - `_mixins.scss` - helper functions
        - `_normalize.scss` - a so called CSS reset that makes styles render more consistently accross diferent browsers
    - `_variables.scss` - contains your custom variables
    - `main.scss` - used to import all the other styles, this is the file that we actually reference later on for styles to appear on the site
- **`css/`** - contains a reference to the main SASS stylesheet
- **`images/`** - images
- `_config.yml` - Jekyll configuration file
- `.gitignore` - list files/folders not to be commited to Git
- `.gitlab-ci.yml` - configuration and instructions for GitLab Pages so it knows how to generate your site
- `404.html` - page not found template
- `about.md` - automatically generates a page on */about*
- `Gemfile` - a list of Ruby gems/packages to be installed when running Jekyll for the first time
- `Gemfile.lock` - autogenerated cache-like file for Ruby gems/packages
- `index.md` - contents of the homepage
- `README.md`- content that will show up on the page of your repository online (like the one you are reading right now)
- `reflections.md` - create a page at /reflections

### Auto-generated files

Jekyll automatically generates a directory `_site` which contains compiled HTML and CSS files that constitute your final site. As a rule of thumb, you should never manually change anything inside this directory.

`.sass-cache` contains SASS cache, as the name suggests. You can ignore this directory. 

If you look in `.gitignore` you can see that all the auto-generated files and directories are not being pushed to an online repository because they are generated server-side when you run a pipeline on GitLab.

### Customisable and arbitrary structure

What you see here is almost entirely arbitrary structure of a Jekyll project. Things like variables and naming of the files is almost entirely a matter of personal preference. This is my way of doing things that has evolved over the years of working with websites. You are encouraged to modify, edit and hack the structure to fit your needs and workflows. Say you would prefer not to have your SASS styles split across many files, you can simply shove all your styles in the ``main.scss`` file.




## Tips and tricks

### Organising your content

When writting markdown try to stick to using only level 2 and level 3 headers to organise your content.

- ``## Header Level 2``
- ``### Header Level 3``

It is assumed that the header level 1 is reserved for the page/post title.

# References

## Git

Definition from [Wikipedia](https://en.wikipedia.org/wiki/Git):

> *Git (/ɡɪt/) is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source-code management in software development, but it can be used to keep track of changes in any set of files. As a distributed revision-control system, it is aimed at speed, data integrity, and support for distributed, non-linear workflows.*

### Learning resources

- [Git homepage](https://git-scm.com/)
- [Git commit best practices](https://medium.com/@nawarpianist/git-commit-best-practices-dab8d722de99)

### Popular git hosts

- [GitLab](https://gitlab.com)
- [GitHub](https://github.com/)
- [BitBucket](https://bitbucket.org/)



## Markdown


Markdown summary from [Wikipedia](https://en.wikipedia.org/wiki/Markdown):

> *Markdown is a lightweight markup language with plain text formatting syntax. It is designed so that it can be converted to HTML and many other formats using a tool by the same name. Markdown is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor. As the initial description of Markdown contained ambiguities and unanswered questions, many implementations and extensions of Markdown appeared over the years to answer these issues.*



John Gruber who initially developed Markdown in 2004 [describes its philosophy](https://daringfireball.net/projects/markdown/syntax#philosophy) as:


> *Markdown is intended to be as easy-to-read and easy-to-write as is feasible.*
>
> *Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions. While Markdown’s syntax has been influenced by several existing text-to-HTML filters […] the single biggest source of inspiration for Markdown’s syntax is the format of plain text email.*
>
> *To this end, Markdown’s syntax is comprised entirely of punctuation characters, which punctuation characters have been carefully chosen so as to look like what they mean. E.g., asterisks around a word actually look like *emphasis*. Markdown lists look like, well, lists. Even blockquotes look like quoted passages of text, assuming you’ve ever used email.*


### Learning resources

- [Mastering Markdown](https://masteringmarkdown.com/) - quick 34 minute mini course
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) - handy markdown reference

### Markdown editors

- [MacDown](https://macdown.uranusjr.com/) [Mac] - live preview of markdown
- [MarkdownPad](http://markdownpad.com/) [Windows] - live preview of markdown
- [Typora](https://typora.io/) [Mac/Windows] - minimalist writing
- [iA Writer](https://ia.net/writer) [Mac - paid] - minimalist writing



## Web Development

### Learning resources

- [HTML & CSS is hard](https://internetingishard.com/html-and-css/) - a friendly web development tutorial for complete beginners
- [Command Line Power User](https://commandlinepoweruser.com/) - video series for web developers on learning a modern command line workflow


### Software

- [Atom](http://atom.io) - code editor from GitHub, has easy-to-use Git interface
- [Visual Studio Code](https://code.visualstudio.com/) - code editor from Microsoft, popular with web developers
- [Sublime Text](https://www.sublimetext.com/) - super fast code editor
- [Hyper](https://hyper.is/) - extensible and fully customisable terminal

### Fonts
- [Google Fonts](https://fonts.google.com/) - free and easiest to get going quikckly
- [FontSquirrel](https://www.fontsquirrel.com/) - a long running source for free web fonts
- [Adobe Typekit](https://fonts.adobe.com/typekit) [PAID] - great source for premium fonts, included in Adobe Cloud subscription
- [FontPair](https://fontpair.co/) - helps you find nice Google Font pairings


## Other
- [List of popular static site generators](https://www.staticgen.com/)
- [A list of Jekyll plugins](https://github.com/planetjekyll/awesome-jekyll-plugins)
